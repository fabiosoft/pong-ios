//
//  GameViewController.swift
//  Pong
//
//  Created by Fabio Nisci on 06/06/17.
//  Copyright © 2017 Fabio Nisci. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import MultipeerConnectivity

class GameViewController: UIViewController {

    let messageService = MessageServiceManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageService.delegate = self
        messageService.startService(messageService: "niscipong")
        
        NotificationCenter.default.addObserver(self, selector: #selector(pongtosend(notification:)), name: NSNotification.Name("pongSent"), object: nil)
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            if let scene = SKScene(fileNamed: "GameScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func pongtosend(notification:Notification){
        if let userinfo = notification.userInfo as? [String: Int]{
            let x = userinfo["x"]!
            let y = userinfo["y"]!
            sendMessage("\(x),\(y)")
        }
    }
    
    func sendMessage(_ message:String){
        messageService.sendToAll(message: message)
    }
}

extension GameViewController: MessageServiceManagerDelegate{
    
    func connectedDevicesChanged(manager: MessageServiceManager, connectedDevices: [String]) {
        print("friends: \(connectedDevices)")
    }
    
    func messageReceived(manager: MessageServiceManager, message: String, peerID: MCPeerID) {
        DispatchQueue.main.async {
            print("> \(message)")
            let position  = message.components(separatedBy: ",")
            let x = Int(position[0])!
            let y = Int(position[1])!
            let info:[String:Int] = [ "x": x, "y": y];
            NotificationCenter.default.post(name: NSNotification.Name("pongArrived"), object: nil, userInfo: info)
        }
    }
}

