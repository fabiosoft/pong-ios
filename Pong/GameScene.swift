//
//  GameScene.swift
//  Pong
//
//  Created by Fabio Nisci on 06/06/17.
//  Copyright © 2017 Fabio Nisci. All rights reserved.
//

import SpriteKit
import GameplayKit


enum PlayerGameType {
    case one
    case multi
}

enum Player{
    case main
    case enemy
}

enum GameType{
    case easy
    case medium
    case hard
}

class Message{
    var messageText:String
    var author:String?
    var isMine:Bool
    
    init(messageText: String, author: String?, isMine:Bool){
        self.messageText = messageText
        self.author = author
        self.isMine = isMine
    }
}
class PongScore{
    var mainPlayerScore:Int = 0
    var enemyPlayerScore:Int = 0
    
    func addScore(player:Player, amount:Int = 1){
        if player == .main{
            mainPlayerScore += amount
        }else if player == .enemy{
            enemyPlayerScore += amount
        }
    }
    
    func resetScore(){
        mainPlayerScore = 0
        enemyPlayerScore = 0
    }
}

class GameScene: SKScene {
    
    private let impulse = 50
    
    private var ball:SKSpriteNode!
    private var enemy:SKSpriteNode!
    private var main:SKSpriteNode!
    
    private var enemyScoreLabel:SKLabelNode!
    private var mainScoreLabel:SKLabelNode!
    
    private var score:PongScore!
    private var gameType:GameType = .easy
    
    var currentGameType:PlayerGameType = .multi
    
    override func didMove(to view: SKView) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(positionArrived(notification:)), name: NSNotification.Name("pongArrived"), object: nil)
        
        enemyScoreLabel = self.childNode(withName: "enemyScoreLabel") as! SKLabelNode
        mainScoreLabel = self.childNode(withName: "mainScoreLabel") as! SKLabelNode
        
        ball = self.childNode(withName: "ball") as! SKSpriteNode
        enemy = self.childNode(withName: "enemy") as! SKSpriteNode
        main = self.childNode(withName: "main") as! SKSpriteNode
        
        let border  = SKPhysicsBody(edgeLoopFrom: self.frame)
        border.friction = 0
        border.restitution = 1
        self.physicsBody = border
        
        startGame()
    }
    
    func positionArrived(notification:Notification){
        if currentGameType == .multi{
            if let userinfo = notification.userInfo as? [String: Int]{
                let location = CGPoint(x: userinfo["x"]!, y: userinfo["y"]!)
                enemy.run(SKAction.moveTo(x: location.x, duration: 0.2))
            }
        }
    }
    
    private func startGame() {
        score = PongScore()
        enemyScoreLabel.text = "\(score.enemyPlayerScore)"
        mainScoreLabel.text = "\(score.mainPlayerScore)"
        ball.physicsBody?.applyImpulse(CGVector(dx: impulse , dy: impulse))
    }
    
    private func addScore(playerWhoWon : SKSpriteNode){
        
        ball.position = CGPoint.zero
        ball.physicsBody?.velocity = CGVector.zero
        
        if playerWhoWon == main {
            score.addScore(player: .main)
            ball.physicsBody?.applyImpulse(CGVector(dx: impulse, dy: impulse))
        }
        else if playerWhoWon == enemy {
            score.addScore(player: .enemy)
            ball.physicsBody?.applyImpulse(CGVector(dx: -impulse, dy: -impulse))
        }
        
        enemyScoreLabel.text = "\(score.enemyPlayerScore)"
        mainScoreLabel.text = "\(score.mainPlayerScore)"

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            if currentGameType == .one {
                if location.y > 0 {
                    enemy.run(SKAction.moveTo(x: location.x, duration: 0.2))
                }
                if location.y < 0 {
                    main.run(SKAction.moveTo(x: location.x, duration: 0.2))
                    let info:[String:Int] = ["x":Int(location.x), "y":Int(location.y)];
                    NotificationCenter.default.post(name: NSNotification.Name("pongSent"), object: nil, userInfo: info)
                }
            }else{
                main.run(SKAction.moveTo(x: location.x, duration: 0.2))
                let info:[String:Int] = ["x":Int(location.x), "y":Int(location.y)];
                NotificationCenter.default.post(name: NSNotification.Name("pongSent"), object: nil, userInfo: info)
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            if currentGameType == .one{
                if location.y > 0 {
                    enemy.run(SKAction.moveTo(x: location.x, duration: 0.2))
                }
                if location.y < 0 {
                    main.run(SKAction.moveTo(x: location.x, duration: 0.2))
                    let info:[String:Int] = ["x":Int(location.x), "y":Int(location.y)];
                    NotificationCenter.default.post(name: NSNotification.Name("pongSent"), object: nil, userInfo: info)
                }
            }else{
                main.run(SKAction.moveTo(x: location.x, duration: 0.2))
                let info:[String:Int] = ["x":Int(location.x), "y":Int(location.y)];
                NotificationCenter.default.post(name: NSNotification.Name("pongSent"), object: nil, userInfo: info)
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        if currentGameType == .one{
            switch gameType {
            case .easy:
                enemy.run(SKAction.moveTo(x: ball.position.x, duration: 1.3))
                break
            case .medium:
                enemy.run(SKAction.moveTo(x: ball.position.x, duration: 1.0))
                break
            case .hard:
                enemy.run(SKAction.moveTo(x: ball.position.x, duration: 0.7))
                break
            }
        }
        
        
        if ball.position.y <= main.position.y - 30 {
            addScore(playerWhoWon: enemy)
        }
        else if ball.position.y >= enemy.position.y + 30 {
            addScore(playerWhoWon: main)
        }
    }
}
